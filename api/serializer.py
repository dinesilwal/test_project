# from rest_framework import serializers
#
# class UserSerializer(serializers.Serializer):
#     name = serializers.CharField(max_length=20)
#     email = serializers.CharField(max_length=20)
#     Address = serializers.CharField(max_length=30)
#     password = serializers.CharField(max_length=10

from rest_framework.serializers import ModelSerializer
from .models import User,Student


class Userserializer(ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'


class Studentserializer(ModelSerializer):
    # user = Userserializer()

    class Meta:
        model = Student
        fields = '__all__'


