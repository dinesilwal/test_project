# from django.shortcuts import render
# from rest_framework.views import APIView
# from rest_framework.response import Response
# from rest_framework import serializers
# from .serializer import UserSerializer
# from rest_framework import status
# from rest_framework import serializers
# from django.contrib.auth.hashers import make_password
# from .models import User

#
# class RestUser(APIView):
#     def get(self,request):
#          user=User.objects.all()
#          serializers_get = UserSerializer(user,many=True)
#          return Response(serializers_get.data, status= status.HTTP_200_OK)
#
#
#
#
#     def post(self,request):
#         # name_got=request.data.get('name')
#         # Address_got=request.data.get('Address')
#         password_get = request.data.pop('password')
#         serializers_post = UserSerializer(data=request.data)
#         if serializers_post.is_valid():
#             validated_data = serializers_post.validated_data
#             user = User.objects.create(**validated_data)
#             user.password =make_password(password=password_get,salt=None,hasher="default")
#             user.save()
#
#         return Response({"msg":"sent"})


## day2 ##
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from .models import User, Student
from .serializer import Userserializer, Studentserializer


class UserView(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = Userserializer


class StudentView(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Student.objects.all()
    serializer_class = Studentserializer


def index_page(request):
    # return HttpResponse("Hello from the other side !!")
    return render(request, 'index.html')
