from django.db import models


class User(models.Model):
    name = models.CharField(max_length=20)
    Address = models.CharField(max_length=30)
    email = models.CharField(max_length=20)
    password = models.CharField(max_length=10, null=True)


class Student(models.Model):
    rollno = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
